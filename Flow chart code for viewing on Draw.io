<mxfile host="app.diagrams.net" modified="2021-01-13T16:25:06.281Z" agent="5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 Edg/87.0.664.75" etag="QBr0InBbETIVz5XVu0I3" version="14.1.9" type="gitlab">
  <diagram id="C5RBs43oDa-KdzZeNtuy" name="Page-1">
    <mxGraphModel dx="1695" dy="482" grid="1" gridSize="10" guides="1" tooltips="1" connect="1" arrows="1" fold="1" page="1" pageScale="1" pageWidth="827" pageHeight="1169" math="0" shadow="0">
      <root>
        <mxCell id="WIyWlLk6GJQsqaUBKTNV-0" />
        <mxCell id="WIyWlLk6GJQsqaUBKTNV-1" parent="WIyWlLk6GJQsqaUBKTNV-0" />
        <mxCell id="WIyWlLk6GJQsqaUBKTNV-2" value="" style="rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;edgeStyle=orthogonalEdgeStyle;" parent="WIyWlLk6GJQsqaUBKTNV-1" source="WIyWlLk6GJQsqaUBKTNV-3" edge="1">
          <mxGeometry relative="1" as="geometry">
            <mxPoint x="220" y="170" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="WIyWlLk6GJQsqaUBKTNV-3" value="start" style="rounded=1;whiteSpace=wrap;html=1;fontSize=12;glass=0;strokeWidth=1;shadow=0;" parent="WIyWlLk6GJQsqaUBKTNV-1" vertex="1">
          <mxGeometry x="160" y="80" width="120" height="40" as="geometry" />
        </mxCell>
        <mxCell id="aWXmjBRcQHlMjvT_c3Bi-5" value="Was add pressed" style="rhombus;whiteSpace=wrap;html=1;" vertex="1" parent="WIyWlLk6GJQsqaUBKTNV-1">
          <mxGeometry x="180" y="170" width="80" height="80" as="geometry" />
        </mxCell>
        <mxCell id="aWXmjBRcQHlMjvT_c3Bi-6" value="yes" style="endArrow=classic;html=1;exitX=1;exitY=0.5;exitDx=0;exitDy=0;" edge="1" parent="WIyWlLk6GJQsqaUBKTNV-1" source="aWXmjBRcQHlMjvT_c3Bi-5">
          <mxGeometry width="50" height="50" relative="1" as="geometry">
            <mxPoint x="390" y="270" as="sourcePoint" />
            <mxPoint x="320" y="210" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="aWXmjBRcQHlMjvT_c3Bi-7" value="Add selected item to basket" style="rounded=0;whiteSpace=wrap;html=1;" vertex="1" parent="WIyWlLk6GJQsqaUBKTNV-1">
          <mxGeometry x="320" y="180" width="120" height="60" as="geometry" />
        </mxCell>
        <mxCell id="aWXmjBRcQHlMjvT_c3Bi-8" value="" style="endArrow=classic;html=1;" edge="1" parent="WIyWlLk6GJQsqaUBKTNV-1">
          <mxGeometry width="50" height="50" relative="1" as="geometry">
            <mxPoint x="440" y="210" as="sourcePoint" />
            <mxPoint x="440" y="210" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="aWXmjBRcQHlMjvT_c3Bi-9" value="" style="endArrow=classic;html=1;exitX=1.017;exitY=0.633;exitDx=0;exitDy=0;exitPerimeter=0;" edge="1" parent="WIyWlLk6GJQsqaUBKTNV-1" source="aWXmjBRcQHlMjvT_c3Bi-7">
          <mxGeometry width="50" height="50" relative="1" as="geometry">
            <mxPoint x="470" y="250" as="sourcePoint" />
            <mxPoint x="520" y="218" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="aWXmjBRcQHlMjvT_c3Bi-10" value="Add the price to the total" style="rounded=0;whiteSpace=wrap;html=1;" vertex="1" parent="WIyWlLk6GJQsqaUBKTNV-1">
          <mxGeometry x="520" y="180" width="120" height="60" as="geometry" />
        </mxCell>
        <mxCell id="aWXmjBRcQHlMjvT_c3Bi-11" value="Add to the reducer of that item" style="rounded=0;whiteSpace=wrap;html=1;" vertex="1" parent="WIyWlLk6GJQsqaUBKTNV-1">
          <mxGeometry x="720" y="180" width="120" height="60" as="geometry" />
        </mxCell>
        <mxCell id="aWXmjBRcQHlMjvT_c3Bi-12" value="" style="endArrow=classic;html=1;exitX=1;exitY=0.5;exitDx=0;exitDy=0;entryX=0;entryY=0.5;entryDx=0;entryDy=0;" edge="1" parent="WIyWlLk6GJQsqaUBKTNV-1" source="aWXmjBRcQHlMjvT_c3Bi-10" target="aWXmjBRcQHlMjvT_c3Bi-11">
          <mxGeometry width="50" height="50" relative="1" as="geometry">
            <mxPoint x="390" y="250" as="sourcePoint" />
            <mxPoint x="440" y="200" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="aWXmjBRcQHlMjvT_c3Bi-13" value="" style="endArrow=classic;html=1;exitX=0.5;exitY=0;exitDx=0;exitDy=0;" edge="1" parent="WIyWlLk6GJQsqaUBKTNV-1" source="aWXmjBRcQHlMjvT_c3Bi-11">
          <mxGeometry width="50" height="50" relative="1" as="geometry">
            <mxPoint x="390" y="250" as="sourcePoint" />
            <mxPoint x="220" y="150" as="targetPoint" />
            <Array as="points">
              <mxPoint x="780" y="150" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="aWXmjBRcQHlMjvT_c3Bi-14" value="No" style="endArrow=classic;html=1;exitX=0.5;exitY=1;exitDx=0;exitDy=0;" edge="1" parent="WIyWlLk6GJQsqaUBKTNV-1" source="aWXmjBRcQHlMjvT_c3Bi-5">
          <mxGeometry width="50" height="50" relative="1" as="geometry">
            <mxPoint x="340" y="240" as="sourcePoint" />
            <mxPoint x="220" y="280" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="aWXmjBRcQHlMjvT_c3Bi-15" value="Was Discount Pressed" style="rhombus;whiteSpace=wrap;html=1;" vertex="1" parent="WIyWlLk6GJQsqaUBKTNV-1">
          <mxGeometry x="170" y="280" width="100" height="80" as="geometry" />
        </mxCell>
        <mxCell id="aWXmjBRcQHlMjvT_c3Bi-18" value="Yes" style="endArrow=classic;html=1;exitX=0;exitY=0.5;exitDx=0;exitDy=0;" edge="1" parent="WIyWlLk6GJQsqaUBKTNV-1" source="aWXmjBRcQHlMjvT_c3Bi-15">
          <mxGeometry width="50" height="50" relative="1" as="geometry">
            <mxPoint x="340" y="230" as="sourcePoint" />
            <mxPoint x="120" y="320" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="aWXmjBRcQHlMjvT_c3Bi-19" value="Generate Discount" style="rounded=0;whiteSpace=wrap;html=1;" vertex="1" parent="WIyWlLk6GJQsqaUBKTNV-1">
          <mxGeometry y="290" width="120" height="60" as="geometry" />
        </mxCell>
        <mxCell id="aWXmjBRcQHlMjvT_c3Bi-21" value="" style="endArrow=classic;html=1;exitX=0.5;exitY=0;exitDx=0;exitDy=0;" edge="1" parent="WIyWlLk6GJQsqaUBKTNV-1" source="aWXmjBRcQHlMjvT_c3Bi-19">
          <mxGeometry width="50" height="50" relative="1" as="geometry">
            <mxPoint x="200" y="310" as="sourcePoint" />
            <mxPoint x="220" y="160" as="targetPoint" />
            <Array as="points">
              <mxPoint x="60" y="160" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="aWXmjBRcQHlMjvT_c3Bi-22" value="Was pay pressed" style="rhombus;whiteSpace=wrap;html=1;" vertex="1" parent="WIyWlLk6GJQsqaUBKTNV-1">
          <mxGeometry x="180" y="380" width="80" height="80" as="geometry" />
        </mxCell>
        <mxCell id="aWXmjBRcQHlMjvT_c3Bi-23" value="" style="endArrow=classic;html=1;exitX=0.5;exitY=1;exitDx=0;exitDy=0;entryX=0.5;entryY=0;entryDx=0;entryDy=0;" edge="1" parent="WIyWlLk6GJQsqaUBKTNV-1" source="aWXmjBRcQHlMjvT_c3Bi-15" target="aWXmjBRcQHlMjvT_c3Bi-22">
          <mxGeometry width="50" height="50" relative="1" as="geometry">
            <mxPoint x="200" y="300" as="sourcePoint" />
            <mxPoint x="250" y="250" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="aWXmjBRcQHlMjvT_c3Bi-24" value="Yes" style="endArrow=classic;html=1;" edge="1" parent="WIyWlLk6GJQsqaUBKTNV-1">
          <mxGeometry width="50" height="50" relative="1" as="geometry">
            <mxPoint x="220" y="460" as="sourcePoint" />
            <mxPoint x="220" y="480" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="aWXmjBRcQHlMjvT_c3Bi-25" value="Load Pay" style="rounded=0;whiteSpace=wrap;html=1;" vertex="1" parent="WIyWlLk6GJQsqaUBKTNV-1">
          <mxGeometry x="160" y="480" width="120" height="60" as="geometry" />
        </mxCell>
        <mxCell id="aWXmjBRcQHlMjvT_c3Bi-26" value="Is amount entered enough" style="rhombus;whiteSpace=wrap;html=1;" vertex="1" parent="WIyWlLk6GJQsqaUBKTNV-1">
          <mxGeometry x="150" y="610" width="140" height="80" as="geometry" />
        </mxCell>
        <mxCell id="aWXmjBRcQHlMjvT_c3Bi-27" value="" style="endArrow=classic;html=1;exitX=0.5;exitY=1;exitDx=0;exitDy=0;entryX=0.5;entryY=0;entryDx=0;entryDy=0;" edge="1" parent="WIyWlLk6GJQsqaUBKTNV-1" source="aWXmjBRcQHlMjvT_c3Bi-25" target="aWXmjBRcQHlMjvT_c3Bi-26">
          <mxGeometry width="50" height="50" relative="1" as="geometry">
            <mxPoint x="200" y="650" as="sourcePoint" />
            <mxPoint x="250" y="600" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="aWXmjBRcQHlMjvT_c3Bi-28" value="No" style="endArrow=classic;html=1;exitX=1;exitY=0.5;exitDx=0;exitDy=0;" edge="1" parent="WIyWlLk6GJQsqaUBKTNV-1" source="aWXmjBRcQHlMjvT_c3Bi-26" target="aWXmjBRcQHlMjvT_c3Bi-29">
          <mxGeometry width="50" height="50" relative="1" as="geometry">
            <mxPoint x="200" y="650" as="sourcePoint" />
            <mxPoint x="350" y="600" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="aWXmjBRcQHlMjvT_c3Bi-29" value="Minus amount from total&amp;nbsp;" style="rounded=0;whiteSpace=wrap;html=1;" vertex="1" parent="WIyWlLk6GJQsqaUBKTNV-1">
          <mxGeometry x="330" y="620" width="120" height="60" as="geometry" />
        </mxCell>
        <mxCell id="aWXmjBRcQHlMjvT_c3Bi-30" value="" style="endArrow=classic;html=1;exitX=1;exitY=0.5;exitDx=0;exitDy=0;" edge="1" parent="WIyWlLk6GJQsqaUBKTNV-1" source="aWXmjBRcQHlMjvT_c3Bi-29" target="aWXmjBRcQHlMjvT_c3Bi-31">
          <mxGeometry width="50" height="50" relative="1" as="geometry">
            <mxPoint x="450" y="660" as="sourcePoint" />
            <mxPoint x="490" y="650" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="aWXmjBRcQHlMjvT_c3Bi-31" value="State that the amount payed isnt enough" style="rounded=0;whiteSpace=wrap;html=1;" vertex="1" parent="WIyWlLk6GJQsqaUBKTNV-1">
          <mxGeometry x="490" y="620" width="120" height="60" as="geometry" />
        </mxCell>
        <mxCell id="aWXmjBRcQHlMjvT_c3Bi-32" value="" style="endArrow=classic;html=1;exitX=0.5;exitY=0;exitDx=0;exitDy=0;" edge="1" parent="WIyWlLk6GJQsqaUBKTNV-1" source="aWXmjBRcQHlMjvT_c3Bi-31">
          <mxGeometry width="50" height="50" relative="1" as="geometry">
            <mxPoint x="200" y="640" as="sourcePoint" />
            <mxPoint x="220" y="560" as="targetPoint" />
            <Array as="points">
              <mxPoint x="550" y="560" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="aWXmjBRcQHlMjvT_c3Bi-33" value="is there enough items in stock" style="rhombus;whiteSpace=wrap;html=1;" vertex="1" parent="WIyWlLk6GJQsqaUBKTNV-1">
          <mxGeometry x="165" y="710" width="110" height="110" as="geometry" />
        </mxCell>
        <mxCell id="aWXmjBRcQHlMjvT_c3Bi-34" value="yes" style="endArrow=classic;html=1;exitX=0.5;exitY=1;exitDx=0;exitDy=0;entryX=0.5;entryY=0;entryDx=0;entryDy=0;" edge="1" parent="WIyWlLk6GJQsqaUBKTNV-1" source="aWXmjBRcQHlMjvT_c3Bi-26" target="aWXmjBRcQHlMjvT_c3Bi-33">
          <mxGeometry width="50" height="50" relative="1" as="geometry">
            <mxPoint x="200" y="620" as="sourcePoint" />
            <mxPoint x="250" y="570" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="aWXmjBRcQHlMjvT_c3Bi-35" value="No" style="endArrow=classic;html=1;exitX=0;exitY=0.5;exitDx=0;exitDy=0;" edge="1" parent="WIyWlLk6GJQsqaUBKTNV-1" source="aWXmjBRcQHlMjvT_c3Bi-33" target="aWXmjBRcQHlMjvT_c3Bi-36">
          <mxGeometry width="50" height="50" relative="1" as="geometry">
            <mxPoint x="200" y="620" as="sourcePoint" />
            <mxPoint x="130" y="765" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="aWXmjBRcQHlMjvT_c3Bi-36" value="State that there is not enough items&amp;nbsp;" style="rounded=0;whiteSpace=wrap;html=1;" vertex="1" parent="WIyWlLk6GJQsqaUBKTNV-1">
          <mxGeometry x="30" y="735" width="90" height="60" as="geometry" />
        </mxCell>
        <mxCell id="aWXmjBRcQHlMjvT_c3Bi-37" value="" style="endArrow=classic;html=1;exitX=0;exitY=0.5;exitDx=0;exitDy=0;" edge="1" parent="WIyWlLk6GJQsqaUBKTNV-1" source="aWXmjBRcQHlMjvT_c3Bi-36">
          <mxGeometry width="50" height="50" relative="1" as="geometry">
            <mxPoint x="200" y="610" as="sourcePoint" />
            <mxPoint x="220" y="130" as="targetPoint" />
            <Array as="points">
              <mxPoint x="-160" y="765" />
              <mxPoint x="-160" y="130" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="aWXmjBRcQHlMjvT_c3Bi-40" value="Is change required" style="rhombus;whiteSpace=wrap;html=1;" vertex="1" parent="WIyWlLk6GJQsqaUBKTNV-1">
          <mxGeometry x="180" y="860" width="80" height="80" as="geometry" />
        </mxCell>
        <mxCell id="aWXmjBRcQHlMjvT_c3Bi-41" value="yes" style="endArrow=classic;html=1;exitX=0.5;exitY=1;exitDx=0;exitDy=0;" edge="1" parent="WIyWlLk6GJQsqaUBKTNV-1" source="aWXmjBRcQHlMjvT_c3Bi-33" target="aWXmjBRcQHlMjvT_c3Bi-40">
          <mxGeometry width="50" height="50" relative="1" as="geometry">
            <mxPoint x="-60" y="920" as="sourcePoint" />
            <mxPoint x="-10" y="870" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="aWXmjBRcQHlMjvT_c3Bi-42" value="Yes" style="endArrow=classic;html=1;exitX=0;exitY=1;exitDx=0;exitDy=0;" edge="1" parent="WIyWlLk6GJQsqaUBKTNV-1" source="aWXmjBRcQHlMjvT_c3Bi-40">
          <mxGeometry width="50" height="50" relative="1" as="geometry">
            <mxPoint x="-60" y="910" as="sourcePoint" />
            <mxPoint x="140" y="980" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="aWXmjBRcQHlMjvT_c3Bi-45" value="No" style="endArrow=classic;html=1;exitX=1;exitY=1;exitDx=0;exitDy=0;" edge="1" parent="WIyWlLk6GJQsqaUBKTNV-1" source="aWXmjBRcQHlMjvT_c3Bi-40" target="aWXmjBRcQHlMjvT_c3Bi-47">
          <mxGeometry width="50" height="50" relative="1" as="geometry">
            <mxPoint x="250" y="910" as="sourcePoint" />
            <mxPoint x="240" y="1080" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="aWXmjBRcQHlMjvT_c3Bi-46" value="Give change" style="rounded=0;whiteSpace=wrap;html=1;" vertex="1" parent="WIyWlLk6GJQsqaUBKTNV-1">
          <mxGeometry x="80" y="980" width="120" height="60" as="geometry" />
        </mxCell>
        <mxCell id="aWXmjBRcQHlMjvT_c3Bi-47" value="End" style="rounded=1;whiteSpace=wrap;html=1;" vertex="1" parent="WIyWlLk6GJQsqaUBKTNV-1">
          <mxGeometry x="180" y="1070" width="120" height="60" as="geometry" />
        </mxCell>
        <mxCell id="aWXmjBRcQHlMjvT_c3Bi-48" value="" style="endArrow=classic;html=1;exitX=0.5;exitY=1;exitDx=0;exitDy=0;entryX=0;entryY=0.5;entryDx=0;entryDy=0;" edge="1" parent="WIyWlLk6GJQsqaUBKTNV-1" source="aWXmjBRcQHlMjvT_c3Bi-46" target="aWXmjBRcQHlMjvT_c3Bi-47">
          <mxGeometry width="50" height="50" relative="1" as="geometry">
            <mxPoint x="-60" y="1070" as="sourcePoint" />
            <mxPoint x="-10" y="1020" as="targetPoint" />
          </mxGeometry>
        </mxCell>
      </root>
    </mxGraphModel>
  </diagram>
</mxfile>
